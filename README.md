I try to find out, what is required to build a distroless docker image for various services like

* [ ] nginx (inprogress)
* [ ] php-fpm 7.x
* [ ] redis
* [ ] may be more, depends on the spare time :)

After that, I'll try to establish an antomated build pipeline to push and update those images regularly fully-automated to the docker hub registry.

All docker images will be configurable via config-injection (to enable helm charts to make those docker images configurable for kubernetes) and to enable docker-compose/docker stack to inject configs via bind-mount.
